const std = @import("std");
const math = std.math;
const RngGen = std.rand.DefaultPrng;

const HEADER = "P6\n{} {}\n255\n";
const WIDTH = 800;
const HEIGHT = 800;
const RADIUS = 5;

var BUFFER: [WIDTH * HEIGHT]Pixel = undefined;

const Pixel = struct {
    r: u8 = 0,
    g: u8 = 0,
    b: u8 = 0,

    fn flatten(self: Pixel) [3]u8 {
        var buf: [3]u8 = undefined;
        buf[0] = self.r;
        buf[1] = self.g;
        buf[2] = self.b;

        return buf;
    }
};

const Point = struct {
    x: usize,
    y: usize,
    px: Pixel,
};

pub fn main() !void {
    const random = getRandomPoints();
    drawVoronoi(&random);
    try ppmToStdout();
}

fn getRandomPoints() [20]Point {
    var points: [20]Point = undefined;
    var rng = RngGen.init(69420);
    var rand = rng.random();

    for (&points) |*point| {
        point.x = rand.uintAtMost(u16, WIDTH);
        point.y = rand.uintAtMost(u16, HEIGHT);
        point.px.r = rand.int(u8);
        point.px.g = rand.int(u8);
        point.px.b = rand.int(u8);
    }

    return points;
}

fn drawVoronoi(points: []const Point) void {
    for (&BUFFER, 0..) |*px, i| {
        const x = i % WIDTH;
        const y = i / WIDTH;

        var pt: Point = undefined;
        var d: usize = WIDTH;

        for (points) |p| {
            var dist = distance(x, y, p.x, p.y) catch WIDTH;

            if (dist < d) {
                d = dist;
                pt = p;
            }
        }

        if (d <= RADIUS) {
            px.r = 0;
            px.g = 0;
            px.b = 0;
        } else {
            px.r = pt.px.r;
            px.g = pt.px.g;
            px.b = pt.px.b;
        }
    }
}

fn distance(x1: usize, y1: usize, x2: usize, y2: usize) !usize {
    const x = if (x2 > x1) x2 - x1 else x1 - x2;
    const y = if (y2 > y1) y2 - y1 else y1 - y2;
    const ans = x *| x +| y *| y;
    return math.sqrt(ans);
}

fn ppmToStdout() !void {
    const stdout_file = std.io.getStdOut().writer();
    var bw = std.io.bufferedWriter(stdout_file);
    const stdout = bw.writer();
    try stdout.print(HEADER, .{ WIDTH, HEIGHT });

    for (&BUFFER) |px| _ = try stdout.write(&px.flatten());
    try bw.flush();
}
